import tensorflow as tf

from layers import position_wise_feed_forward_network, MultiHeadAttention, LayerWrapper, TransformerEmbedding, \
    SinPositionalEncoding


class TransformerLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, heads, ff_dim, cross_attend, rate, norm_type="pre"):
        super(TransformerLayer, self).__init__()
        self.dim = d_model
        self.heads = heads
        self.cross_attend = cross_attend
        self.norm_type = norm_type

        self.sublayers = {
            'self_attn': LayerWrapper(MultiHeadAttention(d_model=self.dim, num_heads=self.heads, rate=rate),
                                      rate=rate, norm_type=norm_type),
            'ffn': LayerWrapper(position_wise_feed_forward_network(dim=self.dim, inner_dim=ff_dim, rate=rate),
                                rate=rate, norm_type=norm_type)
        }
        if self.cross_attend:
            self.sublayers.update(
                {'cross_attn': LayerWrapper(MultiHeadAttention(d_model=self.dim, num_heads=self.heads, rate=rate),
                                            rate=rate, norm_type=norm_type)})

    def call(self, x, memory=None, mask=None, memory_mask=None, cache=None, training=None):
        new_cache = dict()
        cache = dict() if cache is None else cache

        x, layer_cache = self.sublayers['self_attn'](x, mask=mask, cache=cache.get('self_attn'), training=training)
        new_cache.update({'self_attn': layer_cache})
        if self.cross_attend:
            x, layer_cache = self.sublayers['cross_attn'](x, memory=memory, memory_mask=memory_mask,
                                                          cache=cache.get('cross_attn'),
                                                          training=training)
            new_cache.update({'cross_attn': layer_cache})
        out = self.sublayers['ffn'](x, training=training)

        return out, new_cache


class Transformer(tf.keras.layers.Layer):
    def __init__(self, depth, causal, d_model, heads, ff_dim, cross_attend, rate, norm_type="pre"):
        super(Transformer, self).__init__()
        self.depth = depth
        self.dim = d_model
        self.heads = heads
        self.causal = causal
        self.cross_attend = cross_attend
        self.norm_type = norm_type

        self.dropout = tf.keras.layers.Dropout(rate=rate)
        self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6) if self.norm_type == "pre" else None

        self.sublayers = [
            TransformerLayer(d_model=self.dim, heads=self.heads, ff_dim=ff_dim, cross_attend=cross_attend, rate=rate,
                             norm_type=norm_type)
            for _ in range(self.depth)]

    def call(self, inputs, memory=None, mask=None, memory_mask=None, cache=None, training=None):
        new_cache = []
        mask = self.compute_mask(inputs, mask=mask)
        x = inputs
        for i, layer in enumerate(self.sublayers):
            x, layer_cache = layer(x, memory=memory, mask=mask, memory_mask=memory_mask,
                                   cache=cache[i] if cache is not None else None, training=training)
            new_cache.append(layer_cache)
        if self.norm_type == "pre":
            x = self.layernorm(x)
        return x, new_cache

    def compute_mask(self, inputs, mask=None):
        if not self.causal:
            return mask
        batch_size = tf.shape(inputs)[0]
        maximum_length = tf.shape(inputs)[1]
        causal_mask = tf.ones([batch_size, maximum_length, maximum_length], dtype=tf.bool)
        causal_mask = tf.linalg.band_part(causal_mask, -1, 0)
        if mask is not None:
            mask = tf.expand_dims(mask, axis=1)
            return tf.math.logical_and(causal_mask, mask)
        return causal_mask

    def initial_cache(self, batch_size, dtype):
        cache = []
        for _ in self.sublayers:
            shape = [batch_size, self.heads, 0, self.dim // self.heads]
            self_attn = (tf.zeros(shape, dtype=dtype), tf.zeros(shape, dtype=dtype))
            cross_attn = (tf.zeros(shape, dtype=dtype), tf.zeros(shape, dtype=dtype)) if self.cross_attend else None
            cache.append(dict(self_attn=self_attn, cross_attn=cross_attn))
        return cache


class TransformerWrapper(tf.keras.layers.Layer):
    def __init__(self, input_dim, max_seq_len, transformer, emb_rate=0.1):
        super(TransformerWrapper, self).__init__()
        self.embedding = tf.keras.layers.Embedding(input_dim=input_dim, output_dim=transformer.dim, mask_zero=True)
        self.pe = SinPositionalEncoding(dim=transformer.dim, maxlen=max_seq_len)
        self.dropout = tf.keras.layers.Dropout(rate=emb_rate)
        self.scale = tf.math.sqrt(tf.cast(transformer.dim, tf.float32))
        self.transformer = transformer
        self.to_logits = tf.keras.layers.Dense(units=input_dim)

    def call(self, inputs, memory=None, mask=None, memory_mask=None, cache=None, step=None, training=None):
        mask = self.embedding.compute_mask(inputs, mask=mask)
        x = self.embedding(inputs)
        x = self.scale * x
        x = self.pe(x, step=step)
        x = self.dropout(x, training=training)
        x, cache = self.transformer(x, memory=memory, mask=mask, memory_mask=memory_mask, cache=cache,
                                    training=training)
        out = self.to_logits(x)
        return out, cache


class Seq2SeqTransformer(tf.keras.Model):
    def __init__(self, num_layers, d_model, num_heads, dff, enc_embedding, dec_embedding, output_layer,
                 rate=0.1, norm_type="pre"):
        super(Seq2SeqTransformer, self).__init__()
        self.enc_embedding = enc_embedding
        self.dec_embedding = dec_embedding
        self.output_layer = output_layer
        self.norm_type = norm_type
        self.encoder = Transformer(depth=num_layers, causal=False, d_model=d_model,
                                   heads=num_heads, ff_dim=dff, cross_attend=False, rate=rate, norm_type=norm_type)
        self.decoder = Transformer(depth=num_layers, causal=True, d_model=d_model,
                                   heads=num_heads, ff_dim=dff, cross_attend=True, rate=rate, norm_type=norm_type)

    def call(self, source, target, training=None):
        source_mask = self.enc_embedding.compute_mask(source, mask=None)
        source = self.enc_embedding(source, training=training)
        memory, _ = self.encoder(source, mask=source_mask, training=training)

        target_mask = self.dec_embedding.compute_mask(target, mask=None)
        target = self.dec_embedding(target, training=training)
        output, _ = self.decoder(target, memory=memory, mask=target_mask, memory_mask=source_mask, training=training)

        output = self.output_layer(output)
        return output

    def generate(self, source, sos_token, eos_token, max_len):
        memory_mask = self.enc_embedding.compute_mask(source, mask=None)
        source = self.enc_embedding(source, training=False)
        memory, _ = self.encoder(source, mask=memory_mask, training=False)

        batch_size = tf.shape(memory)[0]
        cache = self.decoder.initial_cache(batch_size, dtype=memory.dtype)
        output = tf.fill([batch_size], sos_token)
        finished = tf.zeros([batch_size], dtype=tf.bool)
        outputs = tf.expand_dims(output, 1)

        step = 0
        while tf.reduce_any(tf.logical_not(finished)) and step < max_len:
            x = self.dec_embedding(tf.expand_dims(output, 1), step=step, training=False)
            output, cache = self.decoder(x, memory=memory, memory_mask=memory_mask, cache=cache, training=False)
            logits = self.output_layer(output)
            logits = tf.squeeze(logits, axis=1)
            log_probs = tf.nn.log_softmax(logits)
            sample_scores, sample_ids = tf.nn.top_k(log_probs, k=1)
            output = tf.reshape(sample_ids, [-1])
            outputs = tf.concat([outputs, tf.expand_dims(output, 1)], -1)
            finished = tf.logical_or(finished, tf.equal(output, eos_token))
            step = step + 1
        return outputs


class VanillaTransformer(Seq2SeqTransformer):
    def __init__(self, num_layers, d_model, num_heads, dff, input_vocab_size, target_vocab_size, pe_input, pe_target,
                 rate=0.1, norm_type="pre"):
        self.input_vocab_size = input_vocab_size
        self.target_vocab_size = target_vocab_size
        self.pe_input = pe_input
        self.pe_target = pe_target

        scale = tf.math.sqrt(tf.cast(d_model, tf.float32))

        enc_embedding = TransformerEmbedding(vocab_size=input_vocab_size, dim=d_model, maxlen=pe_input, scale=scale,
                                             rate=rate)
        dec_embedding = TransformerEmbedding(vocab_size=target_vocab_size, dim=d_model, maxlen=pe_target, scale=scale,
                                             rate=rate)
        output_layer = tf.keras.layers.Dense(units=target_vocab_size)

        super(VanillaTransformer, self).__init__(
            num_layers=num_layers, d_model=d_model, num_heads=num_heads, dff=dff,
            enc_embedding=enc_embedding, dec_embedding=dec_embedding, output_layer=output_layer,
            rate=rate, norm_type=norm_type)
