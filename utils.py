import tensorflow as tf


def shape_list(x):
    x = tf.convert_to_tensor(x)
    if x.shape.dims is None:
        return tf.shape(x)
    static = x.shape.as_list()
    shape = tf.shape(x)
    ret = []
    for i, _ in enumerate(static):
        dim = static[i]
        if dim is None:
            dim = shape[i]
        ret.append(dim)
    return ret


#
class NoamDecay(tf.keras.optimizers.schedules.LearningRateSchedule):
    """Defines the lr decay schedule
        lr = d_model ** -0.5 * min(step ** -0.5, step * warmup_steps ** -1.5)
    """

    def __init__(self, scale, d_model, warmup_steps):
        super(NoamDecay, self).__init__()

        self.scale = tf.cast(scale, tf.float32)
        self.d_model = tf.cast(d_model, tf.float32)
        self.warmup_steps = tf.cast(warmup_steps, tf.float32)

    def __call__(self, step):
        step = tf.cast(step + 1, tf.float32)
        return (self.scale
                * tf.pow(self.d_model, -0.5)
                * tf.minimum(tf.pow(step, -0.5), step * tf.pow(self.warmup_steps, -1.5))
                )
