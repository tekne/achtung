* Complete caching implementation for fast decoding
* Support keyword arguments properly
* Implement custom embedding layer to enable sharing, and ensure it masks out padding at output