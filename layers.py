import abc
import numpy as np
import tensorflow as tf

from utils import shape_list


class TransformerEmbedding(tf.keras.layers.Layer):
    def __init__(self, vocab_size, dim, maxlen, pe_type='sinusoid', scale=1.0, rate=0.1):
        super(TransformerEmbedding, self).__init__()
        self.embedding = tf.keras.layers.Embedding(vocab_size, dim, mask_zero=True)
        self.pe = SinPositionalEncoding(dim, maxlen)
        self.scale = scale
        self.dropout = tf.keras.layers.Dropout(rate=rate)

    def call(self, inputs, step=None, training=None):
        x = self.embedding(inputs)
        x = self.scale * x
        x = self.pe(x, step=step)
        x = self.dropout(x, training=training)
        return x

    def compute_mask(self, inputs, mask=None):
        return self.embedding.compute_mask(inputs, mask=mask)


class PositionalEncoding(tf.keras.layers.Layer):
    def __init__(self, dim, maxlen):
        super(PositionalEncoding, self).__init__()
        self.dim = dim
        self.maxlen = maxlen
    
    def call(self, inputs, step=None):
        encoding = self.encode(inputs, step=step)
        return inputs + encoding
        
    @abc.abstractmethod
    def encode(self, inputs, step):
        raise NotImplementedError()


class SinPositionalEncoding(PositionalEncoding):
    def __init__(self, dim, maxlen):
        super(SinPositionalEncoding, self).__init__(dim, maxlen)
        self.pe = positional_encoding(dim, maxlen)

    def encode(self, inputs, step):
        seq_len = tf.shape(inputs)[1]
        if step is None:
            return self.pe[:, :seq_len, :]
        return self.pe[:, step, :]


class LayerWrapper(tf.keras.layers.Layer):
    """
    PreNorm: y = x + dropout(f(layernorm(x)))
    PostNorm: y = layernorm(x + dropout(f(x)))
    """

    def __init__(self, layer, norm_type='pre', rate=0.1, **kwargs):
        super(LayerWrapper, self).__init__(**kwargs)
        self.layer = layer
        self.norm_type = norm_type
        self.rate = rate
        self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.dropout = tf.keras.layers.Dropout(rate=rate)

    def call(self, inputs, *args, **kwargs):
        training = kwargs.get("training")
        x = inputs
        if self.norm_type == 'pre':
            x = self.layernorm(x)
        layer_out = self.layer(x, *args, **kwargs)
        if isinstance(layer_out, tuple):
            outputs = layer_out[0]
            extras = list(layer_out)[1:]
        else:
            outputs = layer_out
            extras = None
        outputs = self.dropout(outputs, training=training)
        outputs = outputs + inputs
        if self.norm_type == 'post':
            outputs = self.layernorm(outputs)
        if extras is not None:
            outputs = tuple([outputs] + extras)
        return outputs


def positional_encoding(d_model, position):
    def get_angles(pos, i):
        angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(d_model))
        return pos * angle_rates

    angle_rads = get_angles(np.arange(position)[:, np.newaxis], np.arange(d_model)[np.newaxis, :])
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]

    return tf.cast(pos_encoding, dtype=tf.float32)


def position_wise_feed_forward_network(dim, inner_dim, rate=0.1):
    return tf.keras.Sequential([
        tf.keras.layers.Dense(inner_dim, activation='relu'),
        tf.keras.layers.Dropout(rate=rate),
        tf.keras.layers.Dense(dim)
    ])


def split_heads(inputs, num_heads):
    """(batch_size, timesteps, dim) -> (batch_size, num_heads, timesteps, dim / num_heads)"""
    shape = shape_list(inputs)
    outputs = tf.reshape(inputs, [shape[0], shape[1], num_heads, shape[2] // num_heads])
    outputs = tf.transpose(outputs, perm=[0, 2, 1, 3])
    return outputs


def concat_heads(inputs):
    shape = shape_list(inputs)
    outputs = tf.transpose(inputs, perm=[0, 2, 1, 3])
    outputs = tf.reshape(outputs, [shape[0], shape[2], shape[1] * shape[3]])
    return outputs


class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(
            self,
            d_model,
            num_heads,
            rate=0.1,
            use_output_bias=True,
            kernel_initializer="glorot_uniform",
            kernel_regularizer=None,
            kernel_constraint=None,
            bias_initializer="zeros",
            bias_regularizer=None,
            bias_constraint=None
    ):
        super(MultiHeadAttention, self).__init__()
        dim = d_model
        dim_head = d_model // num_heads
        self.dim = dim
        self.dim_head = dim_head
        self.num_heads = num_heads
        self.inner_dim = self.dim_head * self.num_heads
        self.dropout = tf.keras.layers.Dropout(rate=rate)

        self.query_proj = tf.keras.layers.Dense(
            units=self.inner_dim,
            use_bias=False,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            kernel_constraint=kernel_constraint
        )
        self.key_proj = tf.keras.layers.Dense(
            units=self.inner_dim,
            use_bias=False,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            kernel_constraint=kernel_constraint
        )
        self.value_proj = tf.keras.layers.Dense(
            units=self.inner_dim,
            use_bias=False,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            kernel_constraint=kernel_constraint
        )
        self.output_proj = tf.keras.layers.Dense(
            units=self.dim,
            use_bias=use_output_bias,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            kernel_regularizer=kernel_regularizer,
            bias_regularizer=bias_regularizer,
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint
        )

    def call(self, x, memory=None, mask=None, memory_mask=None, cache=None, training=None, return_attentions=False):

        def compute_kv(inputs):
            keys = self.key_proj(inputs)
            keys = split_heads(keys, self.num_heads)
            values = self.value_proj(inputs)
            values = split_heads(values, self.num_heads)
            return keys, values

        queries = self.query_proj(x)
        queries = split_heads(queries, self.num_heads)
        queries *= tf.math.rsqrt(tf.cast(self.dim_head, tf.float32))

        if memory is None:
            keys, values = compute_kv(x)
            if cache:
                keys = tf.concat([cache[0], keys], axis=2)
                values = tf.concat([cache[1], values], axis=2)
        else:
            if cache:
                # if first step, compute keys and values
                predicate = tf.equal(tf.shape(cache[0])[2], 0)
                keys, values = tf.cond(predicate, true_fn=lambda: compute_kv(memory), false_fn=lambda: cache)
            else:
                keys, values = compute_kv(memory)

        cache = (keys, values)

        # Dot product attention
        dot = tf.matmul(queries, keys, transpose_b=True)
        if mask is not None:
            mask = tf.cast(mask, tf.float32)
            if mask.shape.rank == 2:
                mask = tf.expand_dims(mask, 1)  # Broadcast on time dimension.
            if mask.shape.rank == 3:
                mask = tf.expand_dims(mask, 1)  # Broadcast on head dimension.
            dot = tf.cast(tf.cast(dot, tf.float32) * mask + ((1.0 - mask) * tf.float32.min), dot.dtype)
        attn = tf.cast(tf.nn.softmax(tf.cast(dot, tf.float32)), dot.dtype)
        drop_attn = self.dropout(attn, training=training)
        heads = tf.matmul(drop_attn, values)
        concatenated = concat_heads(heads)
        outputs = self.output_proj(concatenated)
        if return_attentions:
            return outputs, cache, attn
        return outputs, cache


